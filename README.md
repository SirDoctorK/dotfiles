# README #

## What is this repository for? ##

This repository manages all my personal dotfiles (such as ``.bashrc`` and ``.zshrc``), making it easier to maintain parity between different workstations. To this end, it includes a simple bash script to set up my customized terminal experience on a new workstation. The setup script also installs ``zsh`` (my preferred shell), as well as [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh), [spaceship-prompt](https://github.com/denysdovhan/spaceship-prompt), and [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting), which I use in my ``zshrc``.

The setup script was based on a similar script written by Michael J Smalley for his [dotfiles repository](https://github.com/michaeljsmalley/dotfiles). While my script is heavily modified from his, it was very helpful to have his script to work from.

### Why use Git for dotfiles? ###

Since I work on several different Linux machines regularly and want a consistent experience across all of them (including my customized prompt and alias definitions), Git is an ideal way to accomplish this. By managing these files in a remote Git repository, it is easy to maintain parity between all my workstations. It also makes it much easier to get set up on a new workstation.

Using this method, I have all my managed dotfiles in this one location. On each of my workstations, I simply clone my repository locally into the ``~/repos/dotfiles`` directory. (Please note that my bashrc will throw some errors if the repo is cloned to a different path as it looks there for ``git-completion/git-completion.bash`` and ``git-completion/git-prompt.sh``.) The included setup script will set up zsh according to my personal preferences, although this functionality is easy to change. It will then replace the existing dotfiles in my home directory with symlinks to the dotfiles in my git repository.

Using this method, if I decide to make changes to any of my dotfiles, all I need to do is do a ``git commit`` and ``git push``. At that point, I just need to perform a ``git pull`` and ``source`` the affected dotfiles on all my other machines to receive those changes.

### How do I get set up? ###

If you clone this exact repo and set it up on your box, what you will get is my tailored terminal experience. You can also fork it, modify the rc files and setup script, and use it to set up a customized experience. Note that I am cloning several other repositories in the ``setup_zsh`` function, so if you don't want to use those same ones you'll want to remove them from your version of the script.

I recommend that you create your own empty "dotfiles" repository, then add your own dotfiles. (The script should work even if you name the repository something else, as the script looks for dotiles in its current directory. But keep an eye on it just in case.) Note that the script assumes that the files in the repository DO NOT begin with a "."; instead of placing your ".bashrc" to it, you should make it "bashrc". You can then clone your dotfiles repo into your home directory on a new Linux machine, and once you have modified the script to taste, running the script on a new machine will set everything up for you.

## Setup script information ##

### What does the script do? ###

Here is a detailed description of what this script does if run with no flags and no changes to the code:

Set up zsh:

1. Check to see if ``zsh``, ``git``, and ``curl`` are installed.
    * If any of them aren't, offer to install them
2. Use ``curl`` to run the oh-my-zsh installer script if not already present (and print out a notice to the user regarding the script)
3. Clone spaceship-prompt to ``~/.oh-my-zsh/custom/themes/spaceship-prompt`` (if it's not already present) and set up the necessary symlink for the theme file
4. Check for user-defined spaceship-prompt sections in ``[script-dir]/spaceship_sections``
    * For each such section, check to see if it already exists in ``~/.oh-my-zsh/custom/themes/spaceship-prompt/sections``
    * If it does, move the existing section to ``~/old-dotfiles``
5. Create symlinks from ``~/.oh-my-zsh/custom/themes/spaceship-prompt/sections/section.zsh`` to ``[script-dir]/spaceship_sections/section.zsh``
6. Clone zsh-syntax-highlighting to ``~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting`` (if it's not already present)

Create symlinks for dotfiles:

1. Checks to make sure that zsh setup was successful.
    * If it was not, it asks the user to confirm before continuing
2. Check if ``~/.bashrc`` already exists
    * If it does, move it do ``~/old-dotfiles``
    * Create a symlink from ``~/.bashrc`` to ``[script-dir]/bashrc``
3. Repeat Step 2 for ``zshrc`` and ``bash_profile``

### Command line flags ###

There are several flags you can use when calling the setup script to easily modify some of its behavior. Below is a brief description of each of them:

| Short flag | Long flag | Description |
| ---------- | --------- | ----------- |
| ``-h``     | ``--help``| Help (this screen), then exit |
| ``-o``  | ``--old-dir DIR`` | Directory for backup of old dotfiles (default: $HOME/old-dotfiles) |
| ``-r`` | ``--df-repo DIR`` | Directory of dotfiles repository (default: same directory as script) |
| ``-d``  | ``--delete`` | Delete old files instead of moving them to old-dir |
| ``-s`` | ``--no-symlinks`` | Skip symlink creation |
| ``-z`` | ``--no-zsh`` | Skip checking and setup of zsh (including oh-my-zsh and spaceship-prompt) |
| ``-y`` | ``--yes`` <br> ``--assume-yes`` | Assume 'Yes' to prompts (installing packages; deleting existing dotfiles if requested) |
  
For example, use the following command to call the script to use ``~/my-dotfiles`` as the dotfiles repository and to delete all existing dotfiles:

	./setup.sh -dr ~/my-dotfiles

If these flags are insufficient to get the script to do what you want, or if you find yourself running it with flags every time, you should probably modify the script directly and put your version in your own dotfiles repository.

### Compatibility ###

Almost everything in the script should be platform-independent. That said, I use Ubuntu-based distributions, so that is what I have tested with. The function to install required packages is the most likely to fail on other platforms. It should work on any Debian- or Red hat-based Linux distro. If it is run on an unsupported platform and can't install a required package, it will cancel the script and ask you to install it manually.

## My terminal setup ##

Below is a brief description of how I have set up my preferred terminal settings in the dotfiles set up in this repository.

### zsh ###

I use oh-my-zsh with the spaceschip-prompt theme and zsh-syntax-highlighting plugin. I have several spaceship sections excluded for performance. I've added a nod to Half-Life, an orange lambda, at the start of the prompt, using a custom spaceschip-prompt section defined and managed in this repository. I also have the right prompt set to show the exit code if the last command failed, battery status, and current time. All the other modifications I've made are done by overriding spaceship's default variable values in my ``zshrc``, which are explained [here](https://github.com/denysdovhan/spaceship-prompt/blob/master/docs/Options.md).

### bash ###

While I use zsh as my primary prompt, I have also constructed a custom bash prompt with some similar design cues, since I sometimes use servers that do not have zsh installed. This prompt is declared in my ``bashrc`` in this repository and uses the files in ``git-completion`` for git integration in the prompt.

### aliases ###

For parity between shells, I have all the aliases I use regularly defined in a seperate file. Both ``bashrc`` and ``zshrc`` will import aliases from ``~/repos/dotfiles/aliases``. Since no Linux programs reference this file, I have opted to leave it in dotfiles (rather than symlink it in to ``~``) and let the other rc's look there for it. I also have both rc's set to source aliases recursively from ``~/local_aliases/`` and ``~/local_rc/``.

## Contributing ##

The only file in this repository that even makes sense to receive contributions on is the ``setup.sh`` script. At present I don't know of anything that particularly needs improvement in that script, but I am open to suggestions on it.
