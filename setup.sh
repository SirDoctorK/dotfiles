#!/bin/bash
############################
# Dotfiles and ZSH Setup Script
#
# Written by Stephen Kendall, based on "makesymlinks.sh" by Michael J Smalley
#
# This script:
# Installs zsh, oh-my-zsh, and spaceship-prompt
# Creates symlinks for the dotfiles specified in "files" variable
#
# This script is free software, licensed under the GNU GPL v3
# Feel free to modify this script to fit your specific use case
############################

########## Variables
init_vars () {
  dir=$(dirname "$(readlink -f "$0")")  # dotfiles directory
  olddir=$HOME/old-dotfiles             # old dotfiles backup directory
  files="bashrc zshrc bash_profile"     # list of files/folders to symlink in homedir

  SETZSH=true                           # run setup_zsh function
  SYMLINKS=true                         # run backups_and_symlinks function
  SAVE_OLD=true                         # save old dotfiles and spaceschip sections to olddir
  DELETE=false                          # whether the user has requested to delete old dotfiles

  ZSH_SUCCESS=false                     # true if setup_zsh function was successful

  ASSUME=nothing                        # assumed answer to prompts for pkg installs, deleting dotfiles
}

########## Functions

##### Supporting functions

# Print out brief description of the script and available command line arguments
usage () {
  if [[ $1 != "" ]]; then
    echo -e "\nInvalid option detected"
  else
    echo -e "\nDotfiles and ZSH Setup Script \n\nWritten by Stephen Kendall, based on \"makesymlinks.sh\" by Michael J Smalley"
    echo -e "\nThis script:\nInstalls zsh, oh-my-zsh, and spaceship-prompt\nCreates symlinks for these dotfiles: $files"
    echo -e "\nThis script is free software, licensed under the GNU GPL v3\nFeel free to modify this script to fit your specific use case"
  fi

  echo -e "\nAvailable command line arguments:"

  echo -e "  -h, --help\t\tHelp (this screen), then exit"
  echo -e "  -o, --old-dir DIR\tDirectory for backup of old dotfiles"
  echo -e "\t\t\t(default: \$HOME/old-dotfiles; currently: $olddir)"
  echo -e "  -r, --df-repo DIR\tDirectory of dotfiles repository"
  echo -e "\t\t\t(default: same directory as script; currently: $dir)"
  echo -e "  -d, --delete\t\tDelete old files instead of moving them to old-dir"
  echo -e "  -s, --no-symlinks\tSkip symlink creation"
  echo -e "  -z, --no-zsh\t\tSkip checking and setup of zsh (including oh-my-zsh and spaceship-prompt)"
  echo -e "  -y, --yes, --assume-yes  Assume 'Yes' to prompts (installing packages, deleting existing dotfiles if requested)"
}

# If the -d (delete) flag is set, confirm before changing the variable
confirm_delete () {
  # Check if an assumption has been specified
  if [ $ASSUME == "Y" ]; then
    REPLY=Y
  else
    # Prompt user for confirmation; default to N
    if ! read -p "You have elected to delete your old dotfiles. Are you sure? [y/N]  " -r; then
      # If `read` fails, it's non-interactive and we'll assume no
      echo "Non-interactive, assuming 'no'"
      REPLY=N
    fi
  fi
  if [[ $REPLY =~ ^[Yy] ]]; then
    # If the user DID enter "y*", set variable to delete old files
    echo -e "Okay, old dotfiles will be deleted."
    SAVE_OLD=false
  else
    # If the entered something else (including nothing) save old files
    echo -e "Okay, dotfiles will be saved to $olddir per default."
  fi
}

# Suggest and attempt installation if given package is not installed
install_pkg () {
  # Return 0 if the package is already installed or is successfully installed by this function
  # Return 1 if this function finishes without installing it for any reason

  pkg=$1
  if [[ ! -x "$(command -v $pkg)" ]]; then
    # Check if an assumption has been specified
    if [ $ASSUME == "Y" ]; then
      REPLY=Y
    else
      # Prompt user for confirmation; like apt, default to Y
      if ! read -p "$pkg required but not found, install now? [Y/n] " -r; then
        # If `read` fails, it's non-interactive and we'll assume no
        echo "Non-interactive, assuming 'no'"
        return 1
      fi
    fi
    if [[ $REPLY =~ ^[Yy] ]] || [[ $REPLY == '' ]]; then
      # If the user entered 'y*' or nothing, continue
      echo "Attempting $pkg installation."
      platform="$(uname)"
      # Get OS type and use appropriate command to install pkg
      if [[ $platform == 'Linux' ]]; then
        # sudo prefix to be added if not already root
        SUDO=''
        if (( $EUID != 0 )); then
          SUDO='sudo'
        fi
        # If this file exists, it's probably a Redhat-based distro
        if grep "^ID.*rhel" /etc/os-release; then
          echo "installing with yum"
          # Still using yum here for better backwards compatibility
          $SUDO yum install $pkg -y
        # If this file exists, it's probably a Debian-based distro
        elif grep "^ID.*debian" /etc/os-release; then
          echo "installing with apt-get"
          # Still using apt-get here for better backwards compatibility
          $SUDO apt-get install $pkg -y
        # Identify suse family distros
        elif grep "^ID.*suse" /etc/os-release; then
          echo "installing with zypper"
          $SUDO zypper --non-interactive install $pkg
        fi
      # If the platform is OS X, tell the user to install pkg
      elif [[ $platform == 'Darwin' ]]; then
        echo "Please install $pkg manually, then re-run this script."
        return 1
      else
        echo "Unrecognized platform. Please install $pkg manually, then re-run this script."
        return 1
      fi
    else
      # If the user entered something else, do not proceed
      echo "Cancelling $pkg installation."
      return 1
    fi
  else
    echo "Package $pkg is already installed."
    return 0
  fi

  # Check if the package installed correctly
  if [[ -x "$(command -v $pkg)" ]]; then
    echo "Installation successful"
    return 0
  else
    echo "Installation failed"
    return 1
  fi
}

##### Core functions (in order)

# Install zsh, oh-my-zsh, and spaceschip-prompt
setup_zsh () {
  # Test to see if zsh is installed; attempt to install it if it's not
  if install_pkg "zsh" ; then
    # Git should already be installed, but check anyway; if it isn't, exit the function
    install_pkg "git" || return 1

    # Clone my oh-my-zsh repository from GitHub only if it isn't already present
    if [[ -d $HOME/.oh-my-zsh/ ]]; then
      echo "Already here: oh-my-zsh"
    else
      echo "Installig oh-my-zsh ..."
      # Make sure that curl is installed
      if install_pkg "curl"; then
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" "" --unattended
      else
        return 1
      fi
    fi

    # If not alredy present, clone spaceship-prompt from github
    spaceship_dir=$HOME/.oh-my-zsh/custom/themes/spaceship-prompt
    if [[ -d $spaceship_dir ]]; then
      echo "Already here: spaceship-prompt"
    else
      echo "Installing spaceschip-prompt ..."
      git clone https://github.com/denysdovhan/spaceship-prompt.git $spaceship_dir
      # This symlink is a part of normal spaceschip prompt setup, to ensure that it is available for oh-my-zsh to use as a theme
      ln -s "$spaceship_dir/spaceship.zsh-theme" "$HOME/.oh-my-zsh/custom/themes/spaceship.zsh-theme"
    fi

    # Find all user-defined spaceship sections in dotfiles subdirectory
    sections=$(ls $dir/spaceship_sections)
    for file in $sections; do
      # Check if section file or symlink already exists
      if [[ -f $spaceship_dir/sections/$file ]] || [[ -L $spaceship_dir/sections/$file ]]; then
        # Deal with existing section file
        echo -n "Spaceschip section $file already exists... "
        if [[ "$(readlink $spaceship_dir/sections/$file)" == "$dir/spaceship_sections/$file" ]]; then
          echo "already correct"
        else
          if $SAVE_OLD; then
            echo "moving it to $olddir"
            mv -f $spaceship_dir/sections/$file $olddir
          else
            echo "removing it"
            rm $spaceship_dir/sections/$file
          fi
          # Link section file
          echo "Creating link for $file"
          ln -s $dir/spaceship_sections/$file $spaceship_dir/sections/$file
        fi
      else
        # Link section file
        ln -s $dir/spaceship_sections/$file $spaceship_dir/sections/$file
      fi
    done

    # If not already present, clone zsh-syntax-highlighting from github
    highlight_dir=$HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
    if [[ -d $highlight_dir ]]; then
      echo "Already here: zsh-syntax-highlighting"
    else
      echo "Installing zsh-syntax-highlighting ..."
      git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $highlight_dir
    fi

    echo "Zsh configured. You can set it as default with \"chsh -s $(which zsh)\""
    ZSH_SUCCESS=true
  else
    # If zsh is not installed
    echo "Can't configure zsh because it's not installed. You can re-run the script once it's installed."
    return 1
  fi
}

# Backup existing dotfiles and replace with symlinks
backups_and_symlinks () {
  # If the script attempted to set up zsh but failed, print this warning and confirm before proceeding
  if $SETZSH && ! $ZSH_SUCCESS ; then
    echo "Configuring zsh was unsuccessful, which may prevent .zshrc from working properly."
    # Prompt user for confirmation; default to N
    read -p "Create symlinks anyway? [y/N] " -r
    if [[ $REPLY =~ ^[Yy] ]]; then
      # If the user DID enter "y*", allow the function to continue
      echo "Proceeding with symlink creation."
    else
      # If the user did NOT enter "y," exit this function
      echo "Cancelling symlink creation."
      return 1
    fi
  fi

  # move (or delete) any existing dotfiles in homedir to dotfiles_old directory,
  # then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
  cd $dir
  for file in $files; do
    # Verify that the target file exists
    if [[ -f $file ]]; then
      # Check if dotfile already exists
      if [[ -f $HOME/.$file ]] || [[ -L $HOME/.$file ]]; then
        echo -n "Dotfile .$file already exists... "
        if [[ "$(readlink $HOME/.$file)" == "$dir/$file" ]]; then
          echo "already correct"
        else
          if $SAVE_OLD; then
            echo "moving it to $olddir"
            mv -f $HOME/.$file $olddir
          else
            echo "removing it"
            rm $HOME/.$file
          fi
          echo "Creating symlink to $file in home directory."
          ln -s $dir/$file $HOME/.$file
        fi
      else
        echo "Creating symlink to $file in home directory."
        ln -s $dir/$file $HOME/.$file
      fi
    else
      echo "Can't find $file; skipping..."
    fi
  done
}

########## End of function declarations

########## Actual script

# initialize variables
init_vars

# process command line arguments and override variables as needed
# method for flag parsing from here: https://gist.github.com/shakefu/2765260

# list of single-letter flags supported
opts="ho:r:dszy"

for pass in 1 2; do
  while [ -n "$1" ]; do
    case $1 in
      --) shift; break;;
      -*) case $1 in
        -h | --help )
          usage
          exit
          ;;
        -o | --old-dir )
          shift
          olddir=$1
          ;;
        -r | --df-repo )
          shift
          dir=$1
          ;;
        -d | --delete )
          DELETE=true
          ;;
        -s | --no-symlinks )
          SYMLINKS=false
          ;;
        -z | --no-zsh )
          SETZSH=false
          ;;
        -y | --yes | --assume-yes )
          ASSUME=Y
          ;;
        --*)
          usage $1
          exit
          ;;
        -*)
          if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
          else usage $1; fi;;
        esac;;
      *)  if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
        else error $1; fi;;
    esac
    shift
  done
  if [ $pass -eq 1 ]; then ARGS=$(getopt $opts $ARGS)
    if [ $? != 0 ]; then usage; exit 2; fi; set -- $ARGS
  fi
done

# confirm flag choice to delete old files
if $DELETE; then confirm_delete; fi

# Create old dotfiles dir in homedir
if $SAVE_OLD; then mkdir -p $olddir; fi

# set up zsh and related repositories
if $SETZSH; then setup_zsh; fi

# Create symlinks last so that the oh-my-zsh setup script won't override a user-defined .zshrc
if $SYMLINKS; then backups_and_symlinks; fi
