# switch to zsh if available
#if [ -x "$(command -v zsh)" ]; then
#	exec zsh
#fi

# use .bashrc even over ssh
if [ -f ~/.bashrc ]; then
	source ~/.bashrc
fi
