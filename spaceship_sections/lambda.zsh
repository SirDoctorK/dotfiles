#
# Lambda
#
# Just a little nod to Half-Life that can be placed anywhere in the prompt
#

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------

SPACESHIP_LAMBDA_SHOW="${SPACESHIP_LAMBDA_SHOW=true}"
SPACESHIP_LAMBDA_PREFIX="${SPACESHIP_LAMBDA_PREFIX=""}"
SPACESHIP_LAMBDA_SYMBOL="λ "
SPACESHIP_LAMBDA_SUFFIX="${SPACESHIP_LAMBDA_SUFFIX=""}"
#SPACESHIP_LAMBDA_COLOR="${SPACESHIP_LAMBDA_COLOR="d75f00"}"
SPACESHIP_LAMBDA_COLOR="166"

# ------------------------------------------------------------------------------
# Section
# ------------------------------------------------------------------------------

spaceship_lambda() {
  [[ $SPACESHIP_LAMBDA_SHOW == false ]] && return

#  spaceship::section "$SPACESHIP_LAMBDA_COLOR" "$SPACESHIP_LAMBDA_SYMBOL"

  spaceship::section::v4 \
    --color "$SPACESHIP_LAMBDA_COLOR" \
    --prefix "$SPACESHIP_LAMBDA_PREFIX" \
    --suffix "$SPACESHIP_LAMBDA_SUFFIX" \
    "$SPACESHIP_LAMBDA_SYMBOL"
}
