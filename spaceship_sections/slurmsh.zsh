#
# Slurmsh (slurm shell)
#
# Report a SLURM shell has been opened (typically with 'salloc')
#

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------

SPACESHIP_SLURMSH_SHOW="${SPACESHIP_SLURMSH_SHOW=true}"
SPACESHIP_SLURMSH_PREFIX="${SPACESHIP_SLURMSH_PREFIX="job "}"
SPACESHIP_SLURMSH_SUFFIX="${SPACESHIP_SLURMSH_SUFFIX="$SPACESHIP_PROMPT_DEFAULT_SUFFIX"}"
SPACESHIP_SLURMSH_COLOR="${SPACESHIP_SLURMSH_COLOR="blue"}"
# SPACESHIP_SLURMSH_COLOR="166"

# ------------------------------------------------------------------------------
# Section
# ------------------------------------------------------------------------------

spaceship_slurmsh() {
  [[ $SPACESHIP_SLURMSH_SHOW == false ]] && return

  [[ -z $SLURM_JOBID ]] && return

  local job_details=$(echo "$SLURM_JOBID: $SLURM_NODELIST")

  spaceship::section::v4 \
    --color "$SPACESHIP_SLURMSH_COLOR" \
    --prefix "$SPACESHIP_SLURMSH_PREFIX" \
    --suffix "$SPACESHIP_SLURMSH_SUFFIX" \
    "$job_details"
}
