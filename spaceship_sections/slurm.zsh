#
# Slurm
#
# Track local slurm environment (LOCAL_SLURM should be set elsewhere)
#

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------

SPACESHIP_SLURM_SHOW="${SPACESHIP_SLURM_SHOW=true}"
SPACESHIP_SLURM_PREFIX="${SPACESHIP_SLURM_PREFIX="env "}"
SPACESHIP_SLURM_SUFFIX="${SPACESHIP_SLURM_SUFFIX="$SPACESHIP_PROMPT_DEFAULT_SUFFIX"}"
SPACESHIP_SLURM_COLOR="${SPACESHIP_SLURM_COLOR="magenta"}"
# SPACESHIP_SLURM_COLOR="166"

# ------------------------------------------------------------------------------
# Section
# ------------------------------------------------------------------------------

spaceship_slurm() {
  [[ $SPACESHIP_SLURM_SHOW == false ]] && return

  [[ -z $LOCAL_SLURM ]] && return

  local env_abbrev=$(echo $LOCAL_SLURM | sed "s%$HOME%~%")

  spaceship::section::v4 \
    --color "$SPACESHIP_SLURM_COLOR" \
    --prefix "$SPACESHIP_SLURM_PREFIX" \
    --suffix "$SPACESHIP_SLURM_SUFFIX" \
    "$env_abbrev"
}
